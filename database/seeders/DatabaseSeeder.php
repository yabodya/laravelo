<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Login;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(15)->create()->each(fn(User $user) => $user->logins()->createMany(
            Login::factory(100)->make()->toArray()
        ));

        Category::factory(10)->create()->each(fn(Category $category) => $category->products()->createMany(
            Product::factory(100)->make()->toArray()
        ));
    }
}
