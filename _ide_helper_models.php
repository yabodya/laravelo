<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnusedAliasInspection */

namespace App\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\HasMany;
    use Illuminate\Database\Eloquent\Relations\MorphTo;
    use Illuminate\Database\Eloquent\Relations\MorphToMany;
    use Illuminate\Database\Eloquent\Scope;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Illuminate\Support\Carbon;
    use LaravelIdea\Helper\App\Models\_CategoryCollection;
    use LaravelIdea\Helper\App\Models\_CategoryQueryBuilder;
    use LaravelIdea\Helper\App\Models\_LoginCollection;
    use LaravelIdea\Helper\App\Models\_LoginQueryBuilder;
    use LaravelIdea\Helper\App\Models\_ProductCollection;
    use LaravelIdea\Helper\App\Models\_ProductQueryBuilder;
    use LaravelIdea\Helper\App\Models\_UserCollection;
    use LaravelIdea\Helper\App\Models\_UserQueryBuilder;
    use LaravelIdea\Helper\Database\Factories\_CategoryFactory;
    use LaravelIdea\Helper\Database\Factories\_LoginFactory;
    use LaravelIdea\Helper\Database\Factories\_ProductFactory;
    use LaravelIdea\Helper\Database\Factories\_UserFactory;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationCollection;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationQueryBuilder;

    /**
     * @property int $id
     * @property string $name
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _ProductCollection|Product[] $products
     * @method HasMany|_ProductQueryBuilder products()
     * @method _CategoryQueryBuilder newModelQuery()
     * @method _CategoryQueryBuilder newQuery()
     * @method static _CategoryQueryBuilder query()
     * @method static _CategoryCollection|Category[] all()
     * @method static _CategoryQueryBuilder whereId($value)
     * @method static _CategoryQueryBuilder whereName($value)
     * @method static _CategoryQueryBuilder whereCreatedAt($value)
     * @method static _CategoryQueryBuilder whereUpdatedAt($value)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Category create(array $attributes = [])
     * @method static _CategoryQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _CategoryCollection|Category[] cursor()
     * @method static int decrement(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static bool doesntExistOr(\Closure $callback)
     * @method static _CategoryQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @method static bool exists()
     * @method static bool existsOr(\Closure $callback)
     * @method static Category|null find($id, array $columns = ['*'])
     * @method static _CategoryCollection|Category[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Category findOrFail($id, array $columns = ['*'])
     * @method static _CategoryCollection|Category[] findOrNew($id, array $columns = ['*'])
     * @method static Category first(array|string $columns = ['*'])
     * @method static Category firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Category firstOrCreate(array $attributes = [], array $values = [])
     * @method static Category firstOrFail(array $columns = ['*'])
     * @method static Category firstOrNew(array $attributes = [], array $values = [])
     * @method static Category firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static Category forceCreate(array $attributes)
     * @method static _CategoryCollection|Category[] fromQuery(string $query, array $bindings = [])
     * @method static _CategoryCollection|Category[] get(array|string $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Category getModel()
     * @method static Category[] getModels(array|string $columns = ['*'])
     * @method static _CategoryQueryBuilder getQuery()
     * @method static _CategoryQueryBuilder groupBy(...$groups)
     * @method static bool hasGlobalMacro(string $name)
     * @method static bool hasMacro(string $name)
     * @method static bool hasNamedScope(string $scope)
     * @method static _CategoryCollection|Category[] hydrate(array $items)
     * @method static int increment(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _CategoryQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _CategoryQueryBuilder latest(Expression|string $column = null)
     * @method static _CategoryQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _CategoryQueryBuilder limit(int $value)
     * @method static Category make(array $attributes = [])
     * @method static Category newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _CategoryQueryBuilder offset(int $value)
     * @method static _CategoryQueryBuilder oldest(Expression|string $column = null)
     * @method static _CategoryQueryBuilder orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @method static _CategoryQueryBuilder orderByDesc(string $column)
     * @method static _CategoryQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static LengthAwarePaginator|Category[]|_CategoryCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _CategoryQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _CategoryQueryBuilder select(array $columns = ['*'])
     * @method static _CategoryQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static Paginator|Category[]|_CategoryCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _CategoryQueryBuilder skip(int $value)
     * @method static int sum(string $column)
     * @method static _CategoryQueryBuilder take(int $value)
     * @method static _CategoryQueryBuilder tap(callable $callback)
     * @method static _CategoryQueryBuilder truncate()
     * @method static _CategoryQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Category updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static int upsert(array $values, array|string $uniqueBy, array|null $update = null)
     * @method static _CategoryQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _CategoryQueryBuilder where(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _CategoryQueryBuilder whereDoesntHaveMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null)
     * @method static _CategoryQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _CategoryQueryBuilder whereHasMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _CategoryQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereKey($id)
     * @method static _CategoryQueryBuilder whereKeyNot($id)
     * @method static _CategoryQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNotNull(array|string $columns, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _CategoryQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _CategoryQueryBuilder with(array|string $relations, \Closure|null|string $callback = null)
     * @method static _CategoryQueryBuilder withAggregate($relations, string $column, string $function = null)
     * @method static _CategoryQueryBuilder withAvg(array|string $relation, string $column)
     * @method static _CategoryQueryBuilder withCasts(array $casts)
     * @method static _CategoryQueryBuilder withCount($relations)
     * @method static _CategoryQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _CategoryQueryBuilder withMax(array|string $relation, string $column)
     * @method static _CategoryQueryBuilder withMin(array|string $relation, string $column)
     * @method static _CategoryQueryBuilder withSum(array|string $relation, string $column)
     * @method static _CategoryQueryBuilder without($relations)
     * @method static _CategoryQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _CategoryQueryBuilder withoutGlobalScopes(array $scopes = null)
     * @method static _CategoryFactory factory(...$parameters)
     */
    class Category extends Model
    {
    }

    /**
     * @property int $id
     * @property int $user_id
     * @property string $ip_address
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property User $user
     * @method BelongsTo|_UserQueryBuilder user()
     * @method _LoginQueryBuilder newModelQuery()
     * @method _LoginQueryBuilder newQuery()
     * @method static _LoginQueryBuilder query()
     * @method static _LoginCollection|Login[] all()
     * @method static _LoginQueryBuilder whereId($value)
     * @method static _LoginQueryBuilder whereUserId($value)
     * @method static _LoginQueryBuilder whereIpAddress($value)
     * @method static _LoginQueryBuilder whereCreatedAt($value)
     * @method static _LoginQueryBuilder whereUpdatedAt($value)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Login create(array $attributes = [])
     * @method static _LoginQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _LoginCollection|Login[] cursor()
     * @method static int decrement(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static bool doesntExistOr(\Closure $callback)
     * @method static _LoginQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @method static bool exists()
     * @method static bool existsOr(\Closure $callback)
     * @method static Login|null find($id, array $columns = ['*'])
     * @method static _LoginCollection|Login[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Login findOrFail($id, array $columns = ['*'])
     * @method static _LoginCollection|Login[] findOrNew($id, array $columns = ['*'])
     * @method static Login first(array|string $columns = ['*'])
     * @method static Login firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Login firstOrCreate(array $attributes = [], array $values = [])
     * @method static Login firstOrFail(array $columns = ['*'])
     * @method static Login firstOrNew(array $attributes = [], array $values = [])
     * @method static Login firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static Login forceCreate(array $attributes)
     * @method static _LoginCollection|Login[] fromQuery(string $query, array $bindings = [])
     * @method static _LoginCollection|Login[] get(array|string $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Login getModel()
     * @method static Login[] getModels(array|string $columns = ['*'])
     * @method static _LoginQueryBuilder getQuery()
     * @method static _LoginQueryBuilder groupBy(...$groups)
     * @method static bool hasGlobalMacro(string $name)
     * @method static bool hasMacro(string $name)
     * @method static bool hasNamedScope(string $scope)
     * @method static _LoginCollection|Login[] hydrate(array $items)
     * @method static int increment(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _LoginQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _LoginQueryBuilder latest(Expression|string $column = null)
     * @method static _LoginQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _LoginQueryBuilder limit(int $value)
     * @method static Login make(array $attributes = [])
     * @method static Login newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _LoginQueryBuilder offset(int $value)
     * @method static _LoginQueryBuilder oldest(Expression|string $column = null)
     * @method static _LoginQueryBuilder orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @method static _LoginQueryBuilder orderByDesc(string $column)
     * @method static _LoginQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static LengthAwarePaginator|Login[]|_LoginCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _LoginQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _LoginQueryBuilder select(array $columns = ['*'])
     * @method static _LoginQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static Paginator|Login[]|_LoginCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _LoginQueryBuilder skip(int $value)
     * @method static int sum(string $column)
     * @method static _LoginQueryBuilder take(int $value)
     * @method static _LoginQueryBuilder tap(callable $callback)
     * @method static _LoginQueryBuilder truncate()
     * @method static _LoginQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Login updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static int upsert(array $values, array|string $uniqueBy, array|null $update = null)
     * @method static _LoginQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _LoginQueryBuilder where(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _LoginQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _LoginQueryBuilder whereDoesntHaveMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null)
     * @method static _LoginQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _LoginQueryBuilder whereHasMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _LoginQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereKey($id)
     * @method static _LoginQueryBuilder whereKeyNot($id)
     * @method static _LoginQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNotNull(array|string $columns, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _LoginQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _LoginQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _LoginQueryBuilder with(array|string $relations, \Closure|null|string $callback = null)
     * @method static _LoginQueryBuilder withAggregate($relations, string $column, string $function = null)
     * @method static _LoginQueryBuilder withAvg(array|string $relation, string $column)
     * @method static _LoginQueryBuilder withCasts(array $casts)
     * @method static _LoginQueryBuilder withCount($relations)
     * @method static _LoginQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _LoginQueryBuilder withMax(array|string $relation, string $column)
     * @method static _LoginQueryBuilder withMin(array|string $relation, string $column)
     * @method static _LoginQueryBuilder withSum(array|string $relation, string $column)
     * @method static _LoginQueryBuilder without($relations)
     * @method static _LoginQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _LoginQueryBuilder withoutGlobalScopes(array $scopes = null)
     * @method static _LoginFactory factory(...$parameters)
     */
    class Login extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property int $category_id
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property Category $category
     * @method BelongsTo|_CategoryQueryBuilder category()
     * @method _ProductQueryBuilder newModelQuery()
     * @method _ProductQueryBuilder newQuery()
     * @method static _ProductQueryBuilder query()
     * @method static _ProductCollection|Product[] all()
     * @method static _ProductQueryBuilder whereId($value)
     * @method static _ProductQueryBuilder whereName($value)
     * @method static _ProductQueryBuilder whereCategoryId($value)
     * @method static _ProductQueryBuilder whereCreatedAt($value)
     * @method static _ProductQueryBuilder whereUpdatedAt($value)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static Product create(array $attributes = [])
     * @method static _ProductQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _ProductCollection|Product[] cursor()
     * @method static int decrement(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static bool doesntExistOr(\Closure $callback)
     * @method static _ProductQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @method static bool exists()
     * @method static bool existsOr(\Closure $callback)
     * @method static Product|null find($id, array $columns = ['*'])
     * @method static _ProductCollection|Product[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static Product findOrFail($id, array $columns = ['*'])
     * @method static _ProductCollection|Product[] findOrNew($id, array $columns = ['*'])
     * @method static Product first(array|string $columns = ['*'])
     * @method static Product firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static Product firstOrCreate(array $attributes = [], array $values = [])
     * @method static Product firstOrFail(array $columns = ['*'])
     * @method static Product firstOrNew(array $attributes = [], array $values = [])
     * @method static Product firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static Product forceCreate(array $attributes)
     * @method static _ProductCollection|Product[] fromQuery(string $query, array $bindings = [])
     * @method static _ProductCollection|Product[] get(array|string $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static Product getModel()
     * @method static Product[] getModels(array|string $columns = ['*'])
     * @method static _ProductQueryBuilder getQuery()
     * @method static _ProductQueryBuilder groupBy(...$groups)
     * @method static bool hasGlobalMacro(string $name)
     * @method static bool hasMacro(string $name)
     * @method static bool hasNamedScope(string $scope)
     * @method static _ProductCollection|Product[] hydrate(array $items)
     * @method static int increment(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _ProductQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _ProductQueryBuilder latest(Expression|string $column = null)
     * @method static _ProductQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _ProductQueryBuilder limit(int $value)
     * @method static Product make(array $attributes = [])
     * @method static Product newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _ProductQueryBuilder offset(int $value)
     * @method static _ProductQueryBuilder oldest(Expression|string $column = null)
     * @method static _ProductQueryBuilder orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @method static _ProductQueryBuilder orderByDesc(string $column)
     * @method static _ProductQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static LengthAwarePaginator|Product[]|_ProductCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _ProductQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _ProductQueryBuilder select(array $columns = ['*'])
     * @method static _ProductQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static Paginator|Product[]|_ProductCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _ProductQueryBuilder skip(int $value)
     * @method static int sum(string $column)
     * @method static _ProductQueryBuilder take(int $value)
     * @method static _ProductQueryBuilder tap(callable $callback)
     * @method static _ProductQueryBuilder truncate()
     * @method static _ProductQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static Product updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static int upsert(array $values, array|string $uniqueBy, array|null $update = null)
     * @method static _ProductQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _ProductQueryBuilder where(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _ProductQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _ProductQueryBuilder whereDoesntHaveMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null)
     * @method static _ProductQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _ProductQueryBuilder whereHasMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _ProductQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereKey($id)
     * @method static _ProductQueryBuilder whereKeyNot($id)
     * @method static _ProductQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNotNull(array|string $columns, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _ProductQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _ProductQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _ProductQueryBuilder with(array|string $relations, \Closure|null|string $callback = null)
     * @method static _ProductQueryBuilder withAggregate($relations, string $column, string $function = null)
     * @method static _ProductQueryBuilder withAvg(array|string $relation, string $column)
     * @method static _ProductQueryBuilder withCasts(array $casts)
     * @method static _ProductQueryBuilder withCount($relations)
     * @method static _ProductQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _ProductQueryBuilder withMax(array|string $relation, string $column)
     * @method static _ProductQueryBuilder withMin(array|string $relation, string $column)
     * @method static _ProductQueryBuilder withSum(array|string $relation, string $column)
     * @method static _ProductQueryBuilder without($relations)
     * @method static _ProductQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _ProductQueryBuilder withoutGlobalScopes(array $scopes = null)
     * @method static _ProductFactory factory(...$parameters)
     */
    class Product extends Model
    {
    }

    /**
     * @property int $id
     * @property string $name
     * @property string $email
     * @property Carbon|null $email_verified_at
     * @property string $password
     * @property string|null $remember_token
     * @property Carbon|null $created_at
     * @property Carbon|null $updated_at
     * @property _LoginCollection|Login[] $logins
     * @method HasMany|_LoginQueryBuilder logins()
     * @property _DatabaseNotificationCollection|DatabaseNotification[] $notifications
     * @method MorphToMany|_DatabaseNotificationQueryBuilder notifications()
     * @method _UserQueryBuilder newModelQuery()
     * @method _UserQueryBuilder newQuery()
     * @method static _UserQueryBuilder query()
     * @method static _UserCollection|User[] all()
     * @method static _UserQueryBuilder whereId($value)
     * @method static _UserQueryBuilder whereName($value)
     * @method static _UserQueryBuilder whereEmail($value)
     * @method static _UserQueryBuilder whereEmailVerifiedAt($value)
     * @method static _UserQueryBuilder wherePassword($value)
     * @method static _UserQueryBuilder whereRememberToken($value)
     * @method static _UserQueryBuilder whereCreatedAt($value)
     * @method static _UserQueryBuilder whereUpdatedAt($value)
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static User create(array $attributes = [])
     * @method static _UserQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _UserCollection|User[] cursor()
     * @method static int decrement(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static bool doesntExistOr(\Closure $callback)
     * @method static _UserQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @method static bool exists()
     * @method static bool existsOr(\Closure $callback)
     * @method static User|null find($id, array $columns = ['*'])
     * @method static _UserCollection|User[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static User findOrFail($id, array $columns = ['*'])
     * @method static _UserCollection|User[] findOrNew($id, array $columns = ['*'])
     * @method static User first(array|string $columns = ['*'])
     * @method static User firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static User firstOrCreate(array $attributes = [], array $values = [])
     * @method static User firstOrFail(array $columns = ['*'])
     * @method static User firstOrNew(array $attributes = [], array $values = [])
     * @method static User firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static User forceCreate(array $attributes)
     * @method static _UserCollection|User[] fromQuery(string $query, array $bindings = [])
     * @method static _UserCollection|User[] get(array|string $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static User getModel()
     * @method static User[] getModels(array|string $columns = ['*'])
     * @method static _UserQueryBuilder getQuery()
     * @method static _UserQueryBuilder groupBy(...$groups)
     * @method static bool hasGlobalMacro(string $name)
     * @method static bool hasMacro(string $name)
     * @method static bool hasNamedScope(string $scope)
     * @method static _UserCollection|User[] hydrate(array $items)
     * @method static int increment(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _UserQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _UserQueryBuilder latest(Expression|string $column = null)
     * @method static _UserQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _UserQueryBuilder limit(int $value)
     * @method static User make(array $attributes = [])
     * @method static User newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _UserQueryBuilder offset(int $value)
     * @method static _UserQueryBuilder oldest(Expression|string $column = null)
     * @method static _UserQueryBuilder orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @method static _UserQueryBuilder orderByDesc(string $column)
     * @method static _UserQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static LengthAwarePaginator|User[]|_UserCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _UserQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _UserQueryBuilder select(array $columns = ['*'])
     * @method static _UserQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static Paginator|User[]|_UserCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _UserQueryBuilder skip(int $value)
     * @method static int sum(string $column)
     * @method static _UserQueryBuilder take(int $value)
     * @method static _UserQueryBuilder tap(callable $callback)
     * @method static _UserQueryBuilder truncate()
     * @method static _UserQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static User updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static int upsert(array $values, array|string $uniqueBy, array|null $update = null)
     * @method static _UserQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _UserQueryBuilder where(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _UserQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _UserQueryBuilder whereDoesntHaveMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null)
     * @method static _UserQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _UserQueryBuilder whereHasMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _UserQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _UserQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereKey($id)
     * @method static _UserQueryBuilder whereKeyNot($id)
     * @method static _UserQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNotNull(array|string $columns, string $boolean = 'and')
     * @method static _UserQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _UserQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _UserQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _UserQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _UserQueryBuilder with(array|string $relations, \Closure|null|string $callback = null)
     * @method static _UserQueryBuilder withAggregate($relations, string $column, string $function = null)
     * @method static _UserQueryBuilder withAvg(array|string $relation, string $column)
     * @method static _UserQueryBuilder withCasts(array $casts)
     * @method static _UserQueryBuilder withCount($relations)
     * @method static _UserQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _UserQueryBuilder withMax(array|string $relation, string $column)
     * @method static _UserQueryBuilder withMin(array|string $relation, string $column)
     * @method static _UserQueryBuilder withSum(array|string $relation, string $column)
     * @method static _UserQueryBuilder without($relations)
     * @method static _UserQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _UserQueryBuilder withoutGlobalScopes(array $scopes = null)
     * @method static _UserFactory factory(...$parameters)
     */
    class User extends Model
    {
    }
}

namespace Illuminate\Notifications {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\MorphTo;
    use Illuminate\Database\Eloquent\Scope;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationCollection;
    use LaravelIdea\Helper\Illuminate\Notifications\_DatabaseNotificationQueryBuilder;

    /**
     * @property Model $notifiable
     * @method MorphTo notifiable()
     * @method _DatabaseNotificationQueryBuilder newModelQuery()
     * @method _DatabaseNotificationQueryBuilder newQuery()
     * @method static _DatabaseNotificationQueryBuilder query()
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] all()
     * @method static bool chunk(int $count, callable $callback)
     * @method static bool chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @method static int count(string $columns = '*')
     * @method static DatabaseNotification create(array $attributes = [])
     * @method static _DatabaseNotificationQueryBuilder crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] cursor()
     * @method static int decrement(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool doesntExist()
     * @method static bool doesntExistOr(\Closure $callback)
     * @method static _DatabaseNotificationQueryBuilder each(callable $callback, int $count = 1000)
     * @method static bool eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @method static bool exists()
     * @method static bool existsOr(\Closure $callback)
     * @method static DatabaseNotification|null find($id, array $columns = ['*'])
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method static DatabaseNotification findOrFail($id, array $columns = ['*'])
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] findOrNew($id, array $columns = ['*'])
     * @method static DatabaseNotification first(array|string $columns = ['*'])
     * @method static DatabaseNotification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method static DatabaseNotification firstOrCreate(array $attributes = [], array $values = [])
     * @method static DatabaseNotification firstOrFail(array $columns = ['*'])
     * @method static DatabaseNotification firstOrNew(array $attributes = [], array $values = [])
     * @method static DatabaseNotification firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static DatabaseNotification forceCreate(array $attributes)
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] fromQuery(string $query, array $bindings = [])
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] get(array|string $columns = ['*'])
     * @method static int getCountForPagination(array $columns = ['*'])
     * @method static DatabaseNotification getModel()
     * @method static DatabaseNotification[] getModels(array|string $columns = ['*'])
     * @method static _DatabaseNotificationQueryBuilder getQuery()
     * @method static _DatabaseNotificationQueryBuilder groupBy(...$groups)
     * @method static bool hasGlobalMacro(string $name)
     * @method static bool hasMacro(string $name)
     * @method static bool hasNamedScope(string $scope)
     * @method static _DatabaseNotificationCollection|DatabaseNotification[] hydrate(array $items)
     * @method static int increment(Expression|string $column, float|int $amount = 1, array $extra = [])
     * @method static bool insert(array $values)
     * @method static int insertGetId(array $values, null|string $sequence = null)
     * @method static int insertOrIgnore(array $values)
     * @method static int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @method static _DatabaseNotificationQueryBuilder join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @method static _DatabaseNotificationQueryBuilder latest(Expression|string $column = null)
     * @method static _DatabaseNotificationQueryBuilder leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _DatabaseNotificationQueryBuilder limit(int $value)
     * @method static DatabaseNotification make(array $attributes = [])
     * @method static DatabaseNotification newModelInstance(array $attributes = [])
     * @method static int numericAggregate(string $function, array $columns = ['*'])
     * @method static _DatabaseNotificationQueryBuilder offset(int $value)
     * @method static _DatabaseNotificationQueryBuilder oldest(Expression|string $column = null)
     * @method static _DatabaseNotificationQueryBuilder orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @method static _DatabaseNotificationQueryBuilder orderByDesc(string $column)
     * @method static _DatabaseNotificationQueryBuilder orderByRaw(string $sql, array $bindings = [])
     * @method static LengthAwarePaginator|DatabaseNotification[]|_DatabaseNotificationCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _DatabaseNotificationQueryBuilder rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @method static _DatabaseNotificationQueryBuilder select(array $columns = ['*'])
     * @method static _DatabaseNotificationQueryBuilder setQuery(\Illuminate\Database\Query\Builder $query)
     * @method static Paginator|DatabaseNotification[]|_DatabaseNotificationCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method static _DatabaseNotificationQueryBuilder skip(int $value)
     * @method static int sum(string $column)
     * @method static _DatabaseNotificationQueryBuilder take(int $value)
     * @method static _DatabaseNotificationQueryBuilder tap(callable $callback)
     * @method static _DatabaseNotificationQueryBuilder truncate()
     * @method static _DatabaseNotificationQueryBuilder unless($value, callable $callback, callable|null $default = null)
     * @method static int update(array $values)
     * @method static DatabaseNotification updateOrCreate(array $attributes, array $values = [])
     * @method static bool updateOrInsert(array $attributes, array $values = [])
     * @method static int upsert(array $values, array|string $uniqueBy, array|null $update = null)
     * @method static _DatabaseNotificationQueryBuilder when($value, callable $callback, callable|null $default = null)
     * @method static _DatabaseNotificationQueryBuilder where(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereDoesntHave(string $relation, \Closure $callback = null)
     * @method static _DatabaseNotificationQueryBuilder whereDoesntHaveMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null)
     * @method static _DatabaseNotificationQueryBuilder whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereHas(string $relation, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _DatabaseNotificationQueryBuilder whereHasMorph(MorphTo|string $relation, array|string $types, \Closure $callback = null, string $operator = '>=', int $count = 1)
     * @method static _DatabaseNotificationQueryBuilder whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereKey($id)
     * @method static _DatabaseNotificationQueryBuilder whereKeyNot($id)
     * @method static _DatabaseNotificationQueryBuilder whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNested(\Closure $callback, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotExists(\Closure $callback, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotIn(string $column, $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNotNull(array|string $columns, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @method static _DatabaseNotificationQueryBuilder whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @method static _DatabaseNotificationQueryBuilder with(array|string $relations, \Closure|null|string $callback = null)
     * @method static _DatabaseNotificationQueryBuilder withAggregate($relations, string $column, string $function = null)
     * @method static _DatabaseNotificationQueryBuilder withAvg(array|string $relation, string $column)
     * @method static _DatabaseNotificationQueryBuilder withCasts(array $casts)
     * @method static _DatabaseNotificationQueryBuilder withCount($relations)
     * @method static _DatabaseNotificationQueryBuilder withGlobalScope(string $identifier, \Closure|Scope $scope)
     * @method static _DatabaseNotificationQueryBuilder withMax(array|string $relation, string $column)
     * @method static _DatabaseNotificationQueryBuilder withMin(array|string $relation, string $column)
     * @method static _DatabaseNotificationQueryBuilder withSum(array|string $relation, string $column)
     * @method static _DatabaseNotificationQueryBuilder without($relations)
     * @method static _DatabaseNotificationQueryBuilder withoutGlobalScope(Scope|string $scope)
     * @method static _DatabaseNotificationQueryBuilder withoutGlobalScopes(array $scopes = null)
     * @method static _DatabaseNotificationQueryBuilder read()
     * @method static _DatabaseNotificationQueryBuilder unread()
     */
    class DatabaseNotification extends Model
    {
    }
}

namespace LaravelIdea\Helper {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\ConnectionInterface;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Support\Collection;

    /**
     * @see \Illuminate\Database\Query\Builder::whereIn
     * @method $this whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereNotIn
     * @method $this orWhereNotIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::selectRaw
     * @method $this selectRaw(string $expression, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::insertOrIgnore
     * @method int insertOrIgnore(array $values)
     * @see \Illuminate\Database\Query\Builder::unionAll
     * @method $this unionAll(\Closure|\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::orWhereNull
     * @method $this orWhereNull(string $column)
     * @see \Illuminate\Database\Query\Builder::joinWhere
     * @method $this joinWhere(string $table, \Closure|string $first, string $operator, string $second, string $type = 'inner')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonContains
     * @method $this orWhereJsonContains(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::orderBy
     * @method $this orderBy(\Closure|\Illuminate\Database\Query\Builder|Expression|string $column, string $direction = 'asc')
     * @see \Illuminate\Database\Query\Builder::raw
     * @method Expression raw($value)
     * @see \Illuminate\Database\Concerns\BuildsQueries::each
     * @method $this each(callable $callback, int $count = 1000)
     * @see \Illuminate\Database\Query\Builder::setBindings
     * @method $this setBindings(array $bindings, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonLength
     * @method $this orWhereJsonLength(string $column, $operator, $value = null)
     * @see \Illuminate\Database\Query\Builder::whereRowValues
     * @method $this whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereNotExists
     * @method $this orWhereNotExists(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::orWhereIntegerInRaw
     * @method $this orWhereIntegerInRaw(string $column, array|Arrayable $values)
     * @see \Illuminate\Database\Query\Builder::newQuery
     * @method $this newQuery()
     * @see \Illuminate\Database\Query\Builder::rightJoinSub
     * @method $this rightJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::crossJoin
     * @method $this crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::average
     * @method mixed average(string $column)
     * @see \Illuminate\Database\Query\Builder::existsOr
     * @method $this existsOr(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::sum
     * @method int sum(string $column)
     * @see \Illuminate\Database\Query\Builder::havingRaw
     * @method $this havingRaw(string $sql, array $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getRawBindings
     * @method $this getRawBindings()
     * @see \Illuminate\Database\Query\Builder::orWhereColumn
     * @method $this orWhereColumn(array|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::min
     * @method mixed min(string $column)
     * @see \Illuminate\Support\Traits\Macroable::hasMacro
     * @method $this hasMacro(string $name)
     * @see \Illuminate\Database\Concerns\BuildsQueries::unless
     * @method $this unless($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereNotIn
     * @method $this whereNotIn(string $column, $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereTime
     * @method $this whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::where
     * @method $this where(array|\Closure|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::latest
     * @method $this latest(string $column = 'created_at')
     * @see \Illuminate\Database\Query\Builder::insertUsing
     * @method int insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @see \Illuminate\Database\Query\Builder::rightJoinWhere
     * @method $this rightJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::union
     * @method $this union(\Closure|\Illuminate\Database\Query\Builder $query, bool $all = false)
     * @see \Illuminate\Database\Query\Builder::groupBy
     * @method $this groupBy(...$groups)
     * @see \Illuminate\Database\Query\Builder::orWhereDay
     * @method $this orWhereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::joinSub
     * @method $this joinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::oldest
     * @method $this oldest(string $column = 'created_at')
     * @see \Illuminate\Database\Query\Builder::pluck
     * @method $this pluck(string $column, null|string $key = null)
     * @see \Illuminate\Database\Query\Builder::selectSub
     * @method $this selectSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::dd
     * @method void dd()
     * @see \Illuminate\Database\Query\Builder::whereNull
     * @method $this whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::prepareValueAndOperator
     * @method $this prepareValueAndOperator(string $value, string $operator, bool $useDefault = false)
     * @see \Illuminate\Database\Query\Builder::whereIntegerNotInRaw
     * @method $this whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereRaw
     * @method $this orWhereRaw(string $sql, $bindings = [])
     * @see \Illuminate\Database\Query\Builder::whereJsonContains
     * @method $this whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereBetweenColumns
     * @method $this orWhereBetweenColumns(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::mergeWheres
     * @method $this mergeWheres(array $wheres, array $bindings)
     * @see \Illuminate\Database\Query\Builder::sharedLock
     * @method $this sharedLock()
     * @see \Illuminate\Database\Query\Builder::orderByRaw
     * @method $this orderByRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::simplePaginate
     * @method $this simplePaginate(int $perPage = 15, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @see \Illuminate\Database\Query\Builder::doesntExist
     * @method bool doesntExist()
     * @see \Illuminate\Database\Query\Builder::orWhereMonth
     * @method $this orWhereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::whereNotNull
     * @method $this whereNotNull(array|string $columns, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::count
     * @method int count(string $columns = '*')
     * @see \Illuminate\Database\Query\Builder::orWhereNotBetween
     * @method $this orWhereNotBetween(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::fromRaw
     * @method $this fromRaw(string $expression, $bindings = [])
     * @see \Illuminate\Support\Traits\Macroable::mixin
     * @method $this mixin(object $mixin, bool $replace = true)
     * @see \Illuminate\Database\Query\Builder::take
     * @method $this take(int $value)
     * @see \Illuminate\Database\Query\Builder::orWhereNotBetweenColumns
     * @method $this orWhereNotBetweenColumns(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::updateOrInsert
     * @method $this updateOrInsert(array $attributes, array $values = [])
     * @see \Illuminate\Database\Query\Builder::cursor
     * @method $this cursor()
     * @see \Illuminate\Database\Query\Builder::cloneWithout
     * @method $this cloneWithout(array $properties)
     * @see \Illuminate\Database\Query\Builder::whereBetweenColumns
     * @method $this whereBetweenColumns(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::fromSub
     * @method $this fromSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::update
     * @method $this update(array $values)
     * @see \Illuminate\Database\Query\Builder::cleanBindings
     * @method $this cleanBindings(array $bindings)
     * @see \Illuminate\Database\Query\Builder::orWhereDate
     * @method $this orWhereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::avg
     * @method mixed avg(string $column)
     * @see \Illuminate\Database\Query\Builder::addBinding
     * @method $this addBinding($value, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::getGrammar
     * @method $this getGrammar()
     * @see \Illuminate\Database\Query\Builder::lockForUpdate
     * @method $this lockForUpdate()
     * @see \Illuminate\Database\Concerns\BuildsQueries::eachById
     * @method $this eachById(callable $callback, int $count = 1000, null|string $column = null, null|string $alias = null)
     * @see \Illuminate\Database\Query\Builder::cloneWithoutBindings
     * @method $this cloneWithoutBindings(array $except)
     * @see \Illuminate\Database\Query\Builder::orHavingRaw
     * @method $this orHavingRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::forPageBeforeId
     * @method $this forPageBeforeId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::clone
     * @method $this clone ()
     * @see \Illuminate\Database\Query\Builder::orWhereBetween
     * @method $this orWhereBetween(string $column, array $values)
     * @see \Illuminate\Database\Concerns\ExplainsQueries::explain
     * @method $this explain()
     * @see \Illuminate\Database\Query\Builder::select
     * @method $this select(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::paginate
     * @method $this paginate(int $perPage = 15, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @see \Illuminate\Database\Query\Builder::addSelect
     * @method $this addSelect(array $column)
     * @see \Illuminate\Database\Concerns\BuildsQueries::when
     * @method $this when($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereJsonLength
     * @method $this whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereExists
     * @method $this orWhereExists(\Closure $callback, bool $not = false)
     * @see \Illuminate\Database\Query\Builder::truncate
     * @method $this truncate()
     * @see \Illuminate\Database\Query\Builder::lock
     * @method $this lock(bool|string $value = true)
     * @see \Illuminate\Database\Query\Builder::join
     * @method $this join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::whereMonth
     * @method $this whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::having
     * @method $this having(string $column, null|string $operator = null, null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereNested
     * @method $this whereNested(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::upsert
     * @method $this upsert(array $values, array|string $uniqueBy, array|null $update = null)
     * @see \Illuminate\Database\Query\Builder::orWhereRowValues
     * @method $this orWhereRowValues(array $columns, string $operator, array $values)
     * @see \Illuminate\Database\Query\Builder::useWritePdo
     * @method $this useWritePdo()
     * @see \Illuminate\Database\Query\Builder::orWhereIn
     * @method $this orWhereIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::orderByDesc
     * @method $this orderByDesc(string $column)
     * @see \Illuminate\Database\Query\Builder::orWhereNotNull
     * @method $this orWhereNotNull(string $column)
     * @see \Illuminate\Database\Query\Builder::getProcessor
     * @method $this getProcessor()
     * @see \Illuminate\Database\Query\Builder::increment
     * @method $this increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Query\Builder::skip
     * @method $this skip(int $value)
     * @see \Illuminate\Database\Query\Builder::leftJoinWhere
     * @method $this leftJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::doesntExistOr
     * @method $this doesntExistOr(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::whereNotExists
     * @method $this whereNotExists(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereIntegerInRaw
     * @method $this whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::whereDay
     * @method $this whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::get
     * @method $this get(array|string $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::forNestedWhere
     * @method $this forNestedWhere()
     * @see \Illuminate\Database\Query\Builder::max
     * @method mixed max(string $column)
     * @see \Illuminate\Database\Query\Builder::whereExists
     * @method $this whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::inRandomOrder
     * @method $this inRandomOrder(string $seed = '')
     * @see \Illuminate\Database\Query\Builder::havingBetween
     * @method $this havingBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereYear
     * @method $this orWhereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null)
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunkById
     * @method $this chunkById(int $count, callable $callback, null|string $column = null, null|string $alias = null)
     * @see \Illuminate\Database\Query\Builder::whereDate
     * @method $this whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereJsonDoesntContain
     * @method $this whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::decrement
     * @method $this decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Query\Builder::forPageAfterId
     * @method $this forPageAfterId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::forPage
     * @method $this forPage(int $page, int $perPage = 15)
     * @see \Illuminate\Database\Query\Builder::exists
     * @method bool exists()
     * @see \Illuminate\Support\Traits\Macroable::macroCall
     * @method $this macroCall(string $method, array $parameters)
     * @see \Illuminate\Database\Concerns\BuildsQueries::first
     * @method $this first(array|string $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereColumn
     * @method $this whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::numericAggregate
     * @method $this numericAggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereNotBetween
     * @method $this whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getConnection
     * @method ConnectionInterface getConnection()
     * @see \Illuminate\Database\Query\Builder::mergeBindings
     * @method $this mergeBindings(\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::orWhereJsonDoesntContain
     * @method $this orWhereJsonDoesntContain(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::leftJoinSub
     * @method $this leftJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::find
     * @method $this find(int|string $id, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::crossJoinSub
     * @method $this crossJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::limit
     * @method $this limit(int $value)
     * @see \Illuminate\Database\Query\Builder::from
     * @method $this from(\Closure|\Illuminate\Database\Query\Builder|string $table, null|string $as = null)
     * @see \Illuminate\Database\Query\Builder::whereNotBetweenColumns
     * @method $this whereNotBetweenColumns(string $column, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::insertGetId
     * @method int insertGetId(array $values, null|string $sequence = null)
     * @see \Illuminate\Database\Query\Builder::whereBetween
     * @method $this whereBetween(Expression|string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Concerns\BuildsQueries::tap
     * @method $this tap(callable $callback)
     * @see \Illuminate\Database\Query\Builder::offset
     * @method $this offset(int $value)
     * @see \Illuminate\Database\Query\Builder::addNestedWhereQuery
     * @method $this addNestedWhereQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::rightJoin
     * @method $this rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::leftJoin
     * @method $this leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::insert
     * @method bool insert(array $values)
     * @see \Illuminate\Database\Query\Builder::distinct
     * @method $this distinct()
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunk
     * @method $this chunk(int $count, callable $callback)
     * @see \Illuminate\Database\Query\Builder::reorder
     * @method $this reorder(null|string $column = null, string $direction = 'asc')
     * @see \Illuminate\Database\Query\Builder::whereYear
     * @method $this whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::delete
     * @method $this delete($id = null)
     * @see \Illuminate\Database\Query\Builder::getCountForPagination
     * @method $this getCountForPagination(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::groupByRaw
     * @method $this groupByRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::orWhereIntegerNotInRaw
     * @method $this orWhereIntegerNotInRaw(string $column, array|Arrayable $values)
     * @see \Illuminate\Database\Query\Builder::aggregate
     * @method $this aggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::dump
     * @method void dump()
     * @see \Illuminate\Database\Query\Builder::implode
     * @method $this implode(string $column, string $glue = '')
     * @see \Illuminate\Database\Query\Builder::value
     * @method $this value(string $column)
     * @see \Illuminate\Database\Query\Builder::addWhereExistsQuery
     * @method $this addWhereExistsQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Support\Traits\Macroable::macro
     * @method $this macro(string $name, callable|object $macro)
     * @see \Illuminate\Database\Query\Builder::whereRaw
     * @method $this whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::toSql
     * @method string toSql()
     * @see \Illuminate\Database\Query\Builder::orHaving
     * @method $this orHaving(string $column, null|string $operator = null, null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::getBindings
     * @method array getBindings()
     * @see \Illuminate\Database\Query\Builder::orWhereTime
     * @method $this orWhereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::dynamicWhere
     * @method $this dynamicWhere(string $method, array $parameters)
     * @see \Illuminate\Database\Query\Builder::orWhere
     * @method $this orWhere(array|\Closure|string $column, $operator = null, $value = null)
     */
    class _BaseBuilder extends Builder
    {
    }

    /**
     * @method Collection mapSpread(callable $callback)
     * @method Collection mapWithKeys(callable $callback)
     * @method Collection zip(array $items)
     * @method Collection partition(callable|string $key, $operator = null, $value = null)
     * @method Collection mapInto(string $class)
     * @method Collection mapToGroups(callable $callback)
     * @method Collection map(callable $callback)
     * @method Collection groupBy(array|callable|string $groupBy, bool $preserveKeys = false)
     * @method Collection pluck(array|int|null|string $value, null|string $key = null)
     * @method Collection pad(int $size, $value)
     * @method Collection split(int $numberOfGroups)
     * @method Collection combine($values)
     * @method Collection countBy(callable|string $countBy = null)
     * @method Collection mapToDictionary(callable $callback)
     * @method Collection keys()
     * @method Collection transform(callable $callback)
     * @method Collection flatMap(callable $callback)
     * @method Collection collapse()
     */
    class _BaseCollection extends Collection
    {
    }
}

namespace LaravelIdea\Helper\App\Models {

    use App\Models\Category;
    use App\Models\Login;
    use App\Models\Product;
    use App\Models\User;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Notifications\Notification;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;

    /**
     * @method Category shift()
     * @method Category pop()
     * @method Category get($key, $default = null)
     * @method Category pull($key, $default = null)
     * @method Category first(callable $callback = null, $default = null)
     * @method Category firstWhere(string $key, $operator = null, $value = null)
     * @method Category[] all()
     * @method Category last(callable $callback = null, $default = null)
     * @property-read _CategoryCollectionProxy $keyBy
     * @property-read _CategoryCollectionProxy $partition
     * @property-read _CategoryCollectionProxy $max
     * @property-read _CategoryCollectionProxy $flatMap
     * @property-read _CategoryCollectionProxy $each
     * @property-read _CategoryCollectionProxy $map
     * @property-read _CategoryCollectionProxy $filter
     * @property-read _CategoryCollectionProxy $unique
     * @property-read _CategoryCollectionProxy $groupBy
     * @property-read _CategoryCollectionProxy $sortBy
     * @property-read _CategoryCollectionProxy $contains
     * @property-read _CategoryCollectionProxy $sum
     * @property-read _CategoryCollectionProxy $until
     * @property-read _CategoryCollectionProxy $every
     * @property-read _CategoryCollectionProxy $average
     * @property-read _CategoryCollectionProxy $some
     * @property-read _CategoryCollectionProxy $skipWhile
     * @property-read _CategoryCollectionProxy $sortByDesc
     * @property-read _CategoryCollectionProxy $takeUntil
     * @property-read _CategoryCollectionProxy $avg
     * @property-read _CategoryCollectionProxy $skipUntil
     * @property-read _CategoryCollectionProxy $first
     * @property-read _CategoryCollectionProxy $takeWhile
     * @property-read _CategoryCollectionProxy $min
     * @property-read _CategoryCollectionProxy $reject
     */
    class _CategoryCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Category[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @property _CategoryCollection|mixed $id
     * @property _CategoryCollection|mixed $name
     * @property _CategoryCollection|mixed $created_at
     * @property _CategoryCollection|mixed $updated_at
     * @see \Illuminate\Database\Eloquent\Model::update
     * @method _CategoryCollection update(array $attributes = [], array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::save
     * @method _CategoryCollection save(array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::increment
     * @method _CategoryCollection increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Model::decrement
     * @method _CategoryCollection decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Concerns\HasTimestamps::touch
     * @method _CategoryCollection touch()
     * @see \Illuminate\Database\Eloquent\Model::fill
     * @method _CategoryCollection fill(array $attributes)
     * @see \Illuminate\Database\Eloquent\Model::delete
     * @method _CategoryCollection delete()
     */
    class _CategoryCollectionProxy
    {
    }

    /**
     * @method _CategoryQueryBuilder whereId($value)
     * @method _CategoryQueryBuilder whereName($value)
     * @method _CategoryQueryBuilder whereCreatedAt($value)
     * @method _CategoryQueryBuilder whereUpdatedAt($value)
     * @method Category create(array $attributes = [])
     * @method _CategoryCollection|Category[] cursor()
     * @method Category|null find($id, array $columns = ['*'])
     * @method _CategoryCollection|Category[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Category findOrFail($id, array $columns = ['*'])
     * @method _CategoryCollection|Category[] findOrNew($id, array $columns = ['*'])
     * @method Category first(array|string $columns = ['*'])
     * @method Category firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Category firstOrCreate(array $attributes = [], array $values = [])
     * @method Category firstOrFail(array $columns = ['*'])
     * @method Category firstOrNew(array $attributes = [], array $values = [])
     * @method Category firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Category forceCreate(array $attributes)
     * @method _CategoryCollection|Category[] fromQuery(string $query, array $bindings = [])
     * @method _CategoryCollection|Category[] get(array|string $columns = ['*'])
     * @method Category getModel()
     * @method Category[] getModels(array|string $columns = ['*'])
     * @method _CategoryCollection|Category[] hydrate(array $items)
     * @method Category make(array $attributes = [])
     * @method Category newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Category[]|_CategoryCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Category[]|_CategoryCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Category updateOrCreate(array $attributes, array $values = [])
     */
    class _CategoryQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Login shift()
     * @method Login pop()
     * @method Login get($key, $default = null)
     * @method Login pull($key, $default = null)
     * @method Login first(callable $callback = null, $default = null)
     * @method Login firstWhere(string $key, $operator = null, $value = null)
     * @method Login[] all()
     * @method Login last(callable $callback = null, $default = null)
     * @property-read _LoginCollectionProxy $keyBy
     * @property-read _LoginCollectionProxy $partition
     * @property-read _LoginCollectionProxy $max
     * @property-read _LoginCollectionProxy $flatMap
     * @property-read _LoginCollectionProxy $each
     * @property-read _LoginCollectionProxy $map
     * @property-read _LoginCollectionProxy $filter
     * @property-read _LoginCollectionProxy $unique
     * @property-read _LoginCollectionProxy $groupBy
     * @property-read _LoginCollectionProxy $sortBy
     * @property-read _LoginCollectionProxy $contains
     * @property-read _LoginCollectionProxy $sum
     * @property-read _LoginCollectionProxy $until
     * @property-read _LoginCollectionProxy $every
     * @property-read _LoginCollectionProxy $average
     * @property-read _LoginCollectionProxy $some
     * @property-read _LoginCollectionProxy $skipWhile
     * @property-read _LoginCollectionProxy $sortByDesc
     * @property-read _LoginCollectionProxy $takeUntil
     * @property-read _LoginCollectionProxy $avg
     * @property-read _LoginCollectionProxy $skipUntil
     * @property-read _LoginCollectionProxy $first
     * @property-read _LoginCollectionProxy $takeWhile
     * @property-read _LoginCollectionProxy $min
     * @property-read _LoginCollectionProxy $reject
     */
    class _LoginCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Login[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @property _LoginCollection|mixed $id
     * @property _LoginCollection|mixed $user_id
     * @property _LoginCollection|mixed $ip_address
     * @property _LoginCollection|mixed $created_at
     * @property _LoginCollection|mixed $updated_at
     * @property _LoginCollection|mixed $user
     * @see \Illuminate\Database\Eloquent\Model::update
     * @method _LoginCollection update(array $attributes = [], array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::save
     * @method _LoginCollection save(array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::increment
     * @method _LoginCollection increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Model::decrement
     * @method _LoginCollection decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Concerns\HasTimestamps::touch
     * @method _LoginCollection touch()
     * @see \Illuminate\Database\Eloquent\Model::fill
     * @method _LoginCollection fill(array $attributes)
     * @see \Illuminate\Database\Eloquent\Model::delete
     * @method _LoginCollection delete()
     */
    class _LoginCollectionProxy
    {
    }

    /**
     * @method _LoginQueryBuilder whereId($value)
     * @method _LoginQueryBuilder whereUserId($value)
     * @method _LoginQueryBuilder whereIpAddress($value)
     * @method _LoginQueryBuilder whereCreatedAt($value)
     * @method _LoginQueryBuilder whereUpdatedAt($value)
     * @method Login create(array $attributes = [])
     * @method _LoginCollection|Login[] cursor()
     * @method Login|null find($id, array $columns = ['*'])
     * @method _LoginCollection|Login[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Login findOrFail($id, array $columns = ['*'])
     * @method _LoginCollection|Login[] findOrNew($id, array $columns = ['*'])
     * @method Login first(array|string $columns = ['*'])
     * @method Login firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Login firstOrCreate(array $attributes = [], array $values = [])
     * @method Login firstOrFail(array $columns = ['*'])
     * @method Login firstOrNew(array $attributes = [], array $values = [])
     * @method Login firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Login forceCreate(array $attributes)
     * @method _LoginCollection|Login[] fromQuery(string $query, array $bindings = [])
     * @method _LoginCollection|Login[] get(array|string $columns = ['*'])
     * @method Login getModel()
     * @method Login[] getModels(array|string $columns = ['*'])
     * @method _LoginCollection|Login[] hydrate(array $items)
     * @method Login make(array $attributes = [])
     * @method Login newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Login[]|_LoginCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Login[]|_LoginCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Login updateOrCreate(array $attributes, array $values = [])
     */
    class _LoginQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method Product shift()
     * @method Product pop()
     * @method Product get($key, $default = null)
     * @method Product pull($key, $default = null)
     * @method Product first(callable $callback = null, $default = null)
     * @method Product firstWhere(string $key, $operator = null, $value = null)
     * @method Product[] all()
     * @method Product last(callable $callback = null, $default = null)
     * @property-read _ProductCollectionProxy $keyBy
     * @property-read _ProductCollectionProxy $partition
     * @property-read _ProductCollectionProxy $max
     * @property-read _ProductCollectionProxy $flatMap
     * @property-read _ProductCollectionProxy $each
     * @property-read _ProductCollectionProxy $map
     * @property-read _ProductCollectionProxy $filter
     * @property-read _ProductCollectionProxy $unique
     * @property-read _ProductCollectionProxy $groupBy
     * @property-read _ProductCollectionProxy $sortBy
     * @property-read _ProductCollectionProxy $contains
     * @property-read _ProductCollectionProxy $sum
     * @property-read _ProductCollectionProxy $until
     * @property-read _ProductCollectionProxy $every
     * @property-read _ProductCollectionProxy $average
     * @property-read _ProductCollectionProxy $some
     * @property-read _ProductCollectionProxy $skipWhile
     * @property-read _ProductCollectionProxy $sortByDesc
     * @property-read _ProductCollectionProxy $takeUntil
     * @property-read _ProductCollectionProxy $avg
     * @property-read _ProductCollectionProxy $skipUntil
     * @property-read _ProductCollectionProxy $first
     * @property-read _ProductCollectionProxy $takeWhile
     * @property-read _ProductCollectionProxy $min
     * @property-read _ProductCollectionProxy $reject
     */
    class _ProductCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return Product[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @property _ProductCollection|mixed $id
     * @property _ProductCollection|mixed $name
     * @property _ProductCollection|mixed $category_id
     * @property _ProductCollection|mixed $created_at
     * @property _ProductCollection|mixed $updated_at
     * @property _ProductCollection|mixed $category
     * @see \App\Models\Product::url
     * @method _ProductCollection url()
     * @see \Illuminate\Database\Eloquent\Model::update
     * @method _ProductCollection update(array $attributes = [], array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::save
     * @method _ProductCollection save(array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::increment
     * @method _ProductCollection increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Model::decrement
     * @method _ProductCollection decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Concerns\HasTimestamps::touch
     * @method _ProductCollection touch()
     * @see \Illuminate\Database\Eloquent\Model::fill
     * @method _ProductCollection fill(array $attributes)
     * @see \Illuminate\Database\Eloquent\Model::delete
     * @method _ProductCollection delete()
     */
    class _ProductCollectionProxy
    {
    }

    /**
     * @method _ProductQueryBuilder whereId($value)
     * @method _ProductQueryBuilder whereName($value)
     * @method _ProductQueryBuilder whereCategoryId($value)
     * @method _ProductQueryBuilder whereCreatedAt($value)
     * @method _ProductQueryBuilder whereUpdatedAt($value)
     * @method Product create(array $attributes = [])
     * @method _ProductCollection|Product[] cursor()
     * @method Product|null find($id, array $columns = ['*'])
     * @method _ProductCollection|Product[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Product findOrFail($id, array $columns = ['*'])
     * @method _ProductCollection|Product[] findOrNew($id, array $columns = ['*'])
     * @method Product first(array|string $columns = ['*'])
     * @method Product firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Product firstOrCreate(array $attributes = [], array $values = [])
     * @method Product firstOrFail(array $columns = ['*'])
     * @method Product firstOrNew(array $attributes = [], array $values = [])
     * @method Product firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method Product forceCreate(array $attributes)
     * @method _ProductCollection|Product[] fromQuery(string $query, array $bindings = [])
     * @method _ProductCollection|Product[] get(array|string $columns = ['*'])
     * @method Product getModel()
     * @method Product[] getModels(array|string $columns = ['*'])
     * @method _ProductCollection|Product[] hydrate(array $items)
     * @method Product make(array $attributes = [])
     * @method Product newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Product[]|_ProductCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Product[]|_ProductCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Product updateOrCreate(array $attributes, array $values = [])
     */
    class _ProductQueryBuilder extends _BaseBuilder
    {
    }

    /**
     * @method User shift()
     * @method User pop()
     * @method User get($key, $default = null)
     * @method User pull($key, $default = null)
     * @method User first(callable $callback = null, $default = null)
     * @method User firstWhere(string $key, $operator = null, $value = null)
     * @method User[] all()
     * @method User last(callable $callback = null, $default = null)
     * @property-read _UserCollectionProxy $keyBy
     * @property-read _UserCollectionProxy $partition
     * @property-read _UserCollectionProxy $max
     * @property-read _UserCollectionProxy $flatMap
     * @property-read _UserCollectionProxy $each
     * @property-read _UserCollectionProxy $map
     * @property-read _UserCollectionProxy $filter
     * @property-read _UserCollectionProxy $unique
     * @property-read _UserCollectionProxy $groupBy
     * @property-read _UserCollectionProxy $sortBy
     * @property-read _UserCollectionProxy $contains
     * @property-read _UserCollectionProxy $sum
     * @property-read _UserCollectionProxy $until
     * @property-read _UserCollectionProxy $every
     * @property-read _UserCollectionProxy $average
     * @property-read _UserCollectionProxy $some
     * @property-read _UserCollectionProxy $skipWhile
     * @property-read _UserCollectionProxy $sortByDesc
     * @property-read _UserCollectionProxy $takeUntil
     * @property-read _UserCollectionProxy $avg
     * @property-read _UserCollectionProxy $skipUntil
     * @property-read _UserCollectionProxy $first
     * @property-read _UserCollectionProxy $takeWhile
     * @property-read _UserCollectionProxy $min
     * @property-read _UserCollectionProxy $reject
     */
    class _UserCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return User[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @property _UserCollection|mixed $id
     * @property _UserCollection|mixed $name
     * @property _UserCollection|mixed $email
     * @property _UserCollection|mixed $email_verified_at
     * @property _UserCollection|mixed $password
     * @property _UserCollection|mixed $remember_token
     * @property _UserCollection|mixed $created_at
     * @property _UserCollection|mixed $updated_at
     * @see \Illuminate\Notifications\RoutesNotifications::notifyNow
     * @method _UserCollection notifyNow($instance, array $channels = null)
     * @see \Illuminate\Notifications\RoutesNotifications::notify
     * @method _UserCollection notify($instance)
     * @see \Illuminate\Notifications\RoutesNotifications::routeNotificationFor
     * @method _UserCollection routeNotificationFor(string $driver, Notification|null $notification = null)
     * @see \Illuminate\Notifications\HasDatabaseNotifications::readNotifications
     * @method _UserCollection readNotifications()
     * @see \Illuminate\Notifications\HasDatabaseNotifications::unreadNotifications
     * @method _UserCollection unreadNotifications()
     * @see \Illuminate\Auth\MustVerifyEmail::sendEmailVerificationNotification
     * @method _UserCollection sendEmailVerificationNotification()
     * @see \Illuminate\Auth\MustVerifyEmail::getEmailForVerification
     * @method _UserCollection getEmailForVerification()
     * @see \Illuminate\Auth\MustVerifyEmail::markEmailAsVerified
     * @method _UserCollection markEmailAsVerified()
     * @see \Illuminate\Auth\MustVerifyEmail::hasVerifiedEmail
     * @method _UserCollection hasVerifiedEmail()
     * @see \Illuminate\Auth\Authenticatable::getRememberTokenName
     * @method _UserCollection getRememberTokenName()
     * @see \Illuminate\Auth\Authenticatable::getAuthPassword
     * @method _UserCollection getAuthPassword()
     * @see \Illuminate\Auth\Authenticatable::getAuthIdentifier
     * @method _UserCollection getAuthIdentifier()
     * @see \Illuminate\Auth\Authenticatable::getRememberToken
     * @method _UserCollection getRememberToken()
     * @see \Illuminate\Auth\Authenticatable::setRememberToken
     * @method _UserCollection setRememberToken(string $value)
     * @see \Illuminate\Auth\Authenticatable::getAuthIdentifierName
     * @method _UserCollection getAuthIdentifierName()
     * @see \Illuminate\Foundation\Auth\Access\Authorizable::can
     * @method _UserCollection can(iterable|string $abilities, array $arguments = [])
     * @see \Illuminate\Foundation\Auth\Access\Authorizable::canAny
     * @method _UserCollection canAny(iterable|string $abilities, array $arguments = [])
     * @see \Illuminate\Foundation\Auth\Access\Authorizable::cant
     * @method _UserCollection cant(iterable|string $abilities, array $arguments = [])
     * @see \Illuminate\Foundation\Auth\Access\Authorizable::cannot
     * @method _UserCollection cannot(iterable|string $abilities, array $arguments = [])
     * @see \Illuminate\Auth\Passwords\CanResetPassword::sendPasswordResetNotification
     * @method _UserCollection sendPasswordResetNotification(string $token)
     * @see \Illuminate\Auth\Passwords\CanResetPassword::getEmailForPasswordReset
     * @method _UserCollection getEmailForPasswordReset()
     * @see \Illuminate\Database\Eloquent\Model::update
     * @method _UserCollection update(array $attributes = [], array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::save
     * @method _UserCollection save(array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::increment
     * @method _UserCollection increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Model::decrement
     * @method _UserCollection decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Concerns\HasTimestamps::touch
     * @method _UserCollection touch()
     * @see \Illuminate\Database\Eloquent\Model::fill
     * @method _UserCollection fill(array $attributes)
     * @see \Illuminate\Database\Eloquent\Model::delete
     * @method _UserCollection delete()
     */
    class _UserCollectionProxy
    {
    }

    /**
     * @method _UserQueryBuilder whereId($value)
     * @method _UserQueryBuilder whereName($value)
     * @method _UserQueryBuilder whereEmail($value)
     * @method _UserQueryBuilder whereEmailVerifiedAt($value)
     * @method _UserQueryBuilder wherePassword($value)
     * @method _UserQueryBuilder whereRememberToken($value)
     * @method _UserQueryBuilder whereCreatedAt($value)
     * @method _UserQueryBuilder whereUpdatedAt($value)
     * @method User create(array $attributes = [])
     * @method _UserCollection|User[] cursor()
     * @method User|null find($id, array $columns = ['*'])
     * @method _UserCollection|User[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method User findOrFail($id, array $columns = ['*'])
     * @method _UserCollection|User[] findOrNew($id, array $columns = ['*'])
     * @method User first(array|string $columns = ['*'])
     * @method User firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method User firstOrCreate(array $attributes = [], array $values = [])
     * @method User firstOrFail(array $columns = ['*'])
     * @method User firstOrNew(array $attributes = [], array $values = [])
     * @method User firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method User forceCreate(array $attributes)
     * @method _UserCollection|User[] fromQuery(string $query, array $bindings = [])
     * @method _UserCollection|User[] get(array|string $columns = ['*'])
     * @method User getModel()
     * @method User[] getModels(array|string $columns = ['*'])
     * @method _UserCollection|User[] hydrate(array $items)
     * @method User make(array $attributes = [])
     * @method User newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|User[]|_UserCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|User[]|_UserCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method User updateOrCreate(array $attributes, array $values = [])
     */
    class _UserQueryBuilder extends _BaseBuilder
    {
    }
}

namespace LaravelIdea\Helper\Database\Factories {

    use App\Models\Category;
    use App\Models\Login;
    use App\Models\Product;
    use App\Models\User;
    use Database\Factories\CategoryFactory;
    use Database\Factories\LoginFactory;
    use Database\Factories\ProductFactory;
    use Database\Factories\UserFactory;
    use Illuminate\Database\Eloquent\Model;
    use LaravelIdea\Helper\App\Models\_CategoryCollection;
    use LaravelIdea\Helper\App\Models\_LoginCollection;
    use LaravelIdea\Helper\App\Models\_ProductCollection;
    use LaravelIdea\Helper\App\Models\_UserCollection;

    /**
     * @method Category createOne(array $attributes = [])
     * @method Category makeOne(array|callable $attributes = [])
     * @method _CategoryCollection|Category[]|Category create(array $attributes = [], Model|null $parent = null)
     * @method _CategoryCollection|Category[]|Category make(array $attributes = [], Model|null $parent = null)
     * @method _CategoryCollection|Category[] createMany(iterable $records)
     */
    class _CategoryFactory extends CategoryFactory
    {
    }

    /**
     * @method Login createOne(array $attributes = [])
     * @method Login makeOne(array|callable $attributes = [])
     * @method _LoginCollection|Login[]|Login create(array $attributes = [], Model|null $parent = null)
     * @method _LoginCollection|Login[]|Login make(array $attributes = [], Model|null $parent = null)
     * @method _LoginCollection|Login[] createMany(iterable $records)
     */
    class _LoginFactory extends LoginFactory
    {
    }

    /**
     * @method Product createOne(array $attributes = [])
     * @method Product makeOne(array|callable $attributes = [])
     * @method _ProductCollection|Product[]|Product create(array $attributes = [], Model|null $parent = null)
     * @method _ProductCollection|Product[]|Product make(array $attributes = [], Model|null $parent = null)
     * @method _ProductCollection|Product[] createMany(iterable $records)
     */
    class _ProductFactory extends ProductFactory
    {
    }

    /**
     * @method User createOne(array $attributes = [])
     * @method User makeOne(array|callable $attributes = [])
     * @method _UserCollection|User[]|User create(array $attributes = [], Model|null $parent = null)
     * @method _UserCollection|User[]|User make(array $attributes = [], Model|null $parent = null)
     * @method _UserCollection|User[] createMany(iterable $records)
     */
    class _UserFactory extends UserFactory
    {
    }
}

namespace LaravelIdea\Helper\Illuminate\Notifications {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\Query\Expression;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Notifications\DatabaseNotificationCollection;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;

    /**
     * @method DatabaseNotification shift()
     * @method DatabaseNotification pop()
     * @method DatabaseNotification get($key, $default = null)
     * @method DatabaseNotification pull($key, $default = null)
     * @method DatabaseNotification first(callable $callback = null, $default = null)
     * @method DatabaseNotification firstWhere(string $key, $operator = null, $value = null)
     * @method DatabaseNotification[] all()
     * @method DatabaseNotification last(callable $callback = null, $default = null)
     * @property-read _DatabaseNotificationCollectionProxy $keyBy
     * @property-read _DatabaseNotificationCollectionProxy $partition
     * @property-read _DatabaseNotificationCollectionProxy $max
     * @property-read _DatabaseNotificationCollectionProxy $flatMap
     * @property-read _DatabaseNotificationCollectionProxy $each
     * @property-read _DatabaseNotificationCollectionProxy $map
     * @property-read _DatabaseNotificationCollectionProxy $filter
     * @property-read _DatabaseNotificationCollectionProxy $unique
     * @property-read _DatabaseNotificationCollectionProxy $groupBy
     * @property-read _DatabaseNotificationCollectionProxy $sortBy
     * @property-read _DatabaseNotificationCollectionProxy $contains
     * @property-read _DatabaseNotificationCollectionProxy $sum
     * @property-read _DatabaseNotificationCollectionProxy $until
     * @property-read _DatabaseNotificationCollectionProxy $every
     * @property-read _DatabaseNotificationCollectionProxy $average
     * @property-read _DatabaseNotificationCollectionProxy $some
     * @property-read _DatabaseNotificationCollectionProxy $skipWhile
     * @property-read _DatabaseNotificationCollectionProxy $sortByDesc
     * @property-read _DatabaseNotificationCollectionProxy $takeUntil
     * @property-read _DatabaseNotificationCollectionProxy $avg
     * @property-read _DatabaseNotificationCollectionProxy $skipUntil
     * @property-read _DatabaseNotificationCollectionProxy $first
     * @property-read _DatabaseNotificationCollectionProxy $takeWhile
     * @property-read _DatabaseNotificationCollectionProxy $min
     * @property-read _DatabaseNotificationCollectionProxy $reject
     * @method $this mapWithKeys(callable $callback)
     * @method $this pad(int $size, $value)
     * @method $this keys()
     * @method $this loadMin(array|string $relations, string $column)
     * @method $this loadMissing(array|string $relations)
     * @method $this loadCount(array|string $relations)
     * @method $this merge(array|\ArrayAccess $items)
     * @method $this loadMax(array|string $relations, string $column)
     * @method $this loadMorph(string $relation, array $relations)
     * @method $this loadSum(array|string $relations, string $column)
     * @method $this pluck(array|string $value, null|string $key = null)
     * @method $this map(callable $callback)
     * @method $this unique(callable|null|string $key = null, bool $strict = false)
     * @method $this load(array|string $relations)
     * @method $this diff(array|\ArrayAccess $items)
     * @method $this only($keys)
     * @method $this collapse()
     * @method $this append(array|string $attributes)
     * @method $this makeHidden(array|string $attributes)
     * @method $this flatten(int $depth = INF)
     * @method $this makeVisible(array|string $attributes)
     * @method $this fresh(array|string $with = [])
     * @method $this flip()
     * @method $this intersect(array|\ArrayAccess $items)
     * @method $this except($keys)
     * @method $this loadAvg(array|string $relations, string $column)
     * @method $this zip(array $items)
     * @method $this loadAggregate(array|string $relations, string $column, string $function = null)
     * @method $this loadMorphCount(string $relation, array $relations)
     * @mixin DatabaseNotificationCollection
     */
    class _DatabaseNotificationCollection extends _BaseCollection
    {
        /**
         * @param int $size
         * @return DatabaseNotification[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }

    /**
     * @property _DatabaseNotificationCollection|mixed $notifiable
     * @see \Illuminate\Notifications\DatabaseNotification::markAsRead
     * @method _DatabaseNotificationCollection markAsRead()
     * @see \Illuminate\Notifications\DatabaseNotification::read
     * @method _DatabaseNotificationCollection read()
     * @see \Illuminate\Notifications\DatabaseNotification::markAsUnread
     * @method _DatabaseNotificationCollection markAsUnread()
     * @see \Illuminate\Notifications\DatabaseNotification::unread
     * @method _DatabaseNotificationCollection unread()
     * @see \Illuminate\Database\Eloquent\Model::update
     * @method _DatabaseNotificationCollection update(array $attributes = [], array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::save
     * @method _DatabaseNotificationCollection save(array $options = [])
     * @see \Illuminate\Database\Eloquent\Model::increment
     * @method _DatabaseNotificationCollection increment(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Model::decrement
     * @method _DatabaseNotificationCollection decrement(string $column, float|int $amount = 1, array $extra = [])
     * @see \Illuminate\Database\Eloquent\Concerns\HasTimestamps::touch
     * @method _DatabaseNotificationCollection touch()
     * @see \Illuminate\Database\Eloquent\Model::fill
     * @method _DatabaseNotificationCollection fill(array $attributes)
     * @see \Illuminate\Database\Eloquent\Model::delete
     * @method _DatabaseNotificationCollection delete()
     */
    class _DatabaseNotificationCollectionProxy
    {
    }

    /**
     * @method DatabaseNotification create(array $attributes = [])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] cursor()
     * @method DatabaseNotification|null find($id, array $columns = ['*'])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method DatabaseNotification findOrFail($id, array $columns = ['*'])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] findOrNew($id, array $columns = ['*'])
     * @method DatabaseNotification first(array|string $columns = ['*'])
     * @method DatabaseNotification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method DatabaseNotification firstOrCreate(array $attributes = [], array $values = [])
     * @method DatabaseNotification firstOrFail(array $columns = ['*'])
     * @method DatabaseNotification firstOrNew(array $attributes = [], array $values = [])
     * @method DatabaseNotification firstWhere(array|\Closure|Expression|string $column, $operator = null, $value = null, string $boolean = 'and')
     * @method DatabaseNotification forceCreate(array $attributes)
     * @method _DatabaseNotificationCollection|DatabaseNotification[] fromQuery(string $query, array $bindings = [])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] get(array|string $columns = ['*'])
     * @method DatabaseNotification getModel()
     * @method DatabaseNotification[] getModels(array|string $columns = ['*'])
     * @method _DatabaseNotificationCollection|DatabaseNotification[] hydrate(array $items)
     * @method DatabaseNotification make(array $attributes = [])
     * @method DatabaseNotification newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|DatabaseNotification[]|_DatabaseNotificationCollection paginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|DatabaseNotification[]|_DatabaseNotificationCollection simplePaginate(int|null $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method DatabaseNotification updateOrCreate(array $attributes, array $values = [])
     * @method _DatabaseNotificationQueryBuilder read()
     * @method _DatabaseNotificationQueryBuilder unread()
     */
    class _DatabaseNotificationQueryBuilder extends _BaseBuilder
    {
    }
}