<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LoginController;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logins', [LoginController::class, 'index']);

Route::get('/categories/{category}', [CategoryController::class, 'show']);
Route::get('/products/{category}/{product}', function (Category $category, Product $product) {
    return $product->name;
})->name('product');
